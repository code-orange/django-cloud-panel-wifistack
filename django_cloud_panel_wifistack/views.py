from django.contrib import messages
from django.contrib.gis.geos import Point
from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader
from django.utils.translation import gettext as _

from django_cloud_panel_main.django_cloud_panel_main.views import get_nav
from django_cloud_panel_wifistack.django_cloud_panel_wifistack.forms import *
from django_session_ldap_attributes.django_session_ldap_attributes.decorators import (
    admin_group_required,
)
from django_session_ldap_attributes.django_session_ldap_attributes.snippets import *
from django_wifistack_client.django_wifistack_client.func import *


@admin_group_required("WLAN")
def list_nodes(request):
    template = loader.get_template("django_cloud_panel_wifistack/list_nodes.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Network Management")
    template_opts["content_title_sub"] = _("List Nodes")

    template_opts["menu_groups"] = get_nav(request)

    mdat = get_mdat_customer_for_user(request.user)
    template_opts["all_nodes"] = wifistack_get_wireless_nodes(mdat.id)

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("WLAN")
def edit_node(request, node_id):
    template = loader.get_template("django_cloud_panel_wifistack/edit_node.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Network Management")
    template_opts["content_title_sub"] = _("Edit Node")

    template_opts["menu_groups"] = get_nav(request)

    mdat = get_mdat_customer_for_user(request.user)
    template_opts["node"] = wifistack_get_wireless_node(mdat.id, node_id)

    if request.method == "POST":
        edit_node_form = wifistack_edit_node_form(request.POST, user=request.user)
        if edit_node_form.is_valid():
            updated_fields = dict()
            for field in edit_node_form.fields:
                if field == "wireless_node_map":
                    updated_fields["wireless_node_geo_long"] = (
                        edit_node_form.cleaned_data[field][0]
                    )
                    updated_fields["wireless_node_geo_lat"] = (
                        edit_node_form.cleaned_data[field][1]
                    )
                else:
                    if "_enabled" in field or "wireless_node_lan_bridge" in field:
                        if edit_node_form.cleaned_data[field] is True:
                            updated_fields[field] = "1"
                        else:
                            updated_fields[field] = "0"
                    else:
                        updated_fields[field] = edit_node_form.cleaned_data[field]

            api_response = wifistack_update_wireless_node(
                mdat.id, node_id, updated_fields
            )
            if api_response is False:
                messages.error(
                    request,
                    _("WLAN-Node could not be updated"),
                )
            else:
                messages.success(request, _("WLAN-Node updated"))
                return HttpResponseRedirect("/wifistack/nodes")

    edit_node_form = wifistack_edit_node_form(user=request.user)

    edit_node_form.fields["wireless_node_map"].initial = Point(
        template_opts["node"]["wireless_node_geo_long"],
        template_opts["node"]["wireless_node_geo_lat"],
    )

    for key in template_opts["node"]:
        if key in edit_node_form.fields:
            if key.endswith("_enabled") or key == "wireless_node_lan_bridge":
                if template_opts["node"][key] == "1":
                    edit_node_form.fields[key].initial = True
                else:
                    edit_node_form.fields[key].initial = False
            else:
                edit_node_form.fields[key].initial = template_opts["node"][key]

    template_opts["edit_node_form"] = edit_node_form

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("WLAN")
def modal_dump_info(request, node_id):
    template = loader.get_template("django_cloud_panel_wifistack/modal_dump_info.html")
    template_opts = dict()

    template_opts["content_title_main"] = _("Network Management")
    template_opts["content_title_sub"] = _("Dump Info")

    mdat = get_mdat_customer_for_user(request.user)
    template_opts["dumped_info"] = wifistack_get_wireless_node(mdat.id, node_id)

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("WLAN")
def modal_claim_node(request):
    template = loader.get_template("django_cloud_panel_wifistack/modal_claim_node.html")
    template_opts = dict()

    template_opts["content_title_main"] = _("Network Management")
    template_opts["content_title_sub"] = _("Claim Node")

    mdat = get_mdat_customer_for_user(request.user)
    claim_node_form = wifistack_claim_node_form(user=request.user)
    template_opts["claim_node_form"] = claim_node_form

    return HttpResponse(template.render(template_opts, request))
