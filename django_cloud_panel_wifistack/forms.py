from braces.forms import UserKwargModelFormMixin
from django import forms
from leaflet.forms.fields import PointField
from leaflet.forms.widgets import LeafletWidget
from django.utils.translation import gettext as _

from django_mdat_customer.django_mdat_customer.models import MdatCustomers
from django_wifistack_client.django_wifistack_client.func import wifistack_get_domains


class wifistack_edit_node_form(UserKwargModelFormMixin, forms.Form):
    wireless_node_title = forms.CharField(
        label=_("Title"),
        widget=forms.TextInput(attrs={"class": "form-control col-md-12"}),
        required=False,
    )
    hostname = forms.CharField(
        label=_("Hostname"),
        widget=forms.TextInput(
            attrs={"class": "form-control col-md-12", "readonly": "true"}
        ),
    )
    wireless_node_domain = forms.ChoiceField(
        label=_("Domain"),
        widget=forms.Select(attrs={"class": "form-control col-md-12"}),
    )
    wireless_node_lldp_enabled = forms.BooleanField(
        widget=forms.CheckboxInput(attrs={"class": "custom-control custom-switch"}),
        label=_("LLDP Enabled"),
        required=False,
    )
    wireless_node_snmp_enabled = forms.BooleanField(
        widget=forms.CheckboxInput(attrs={"class": "custom-control custom-switch"}),
        label=_("SNMP Enabled"),
        required=False,
    )
    wireless_node_mesh_on_lan_enabled = forms.BooleanField(
        widget=forms.CheckboxInput(attrs={"class": "custom-control custom-switch"}),
        label=_("Mesh on LAN Enabled"),
        required=False,
    )
    wireless_node_lan_bridge = forms.BooleanField(
        widget=forms.CheckboxInput(attrs={"class": "custom-control custom-switch"}),
        label=_("LAN Bridge Enabled"),
        required=False,
    )
    wireless_node_map = PointField()

    class Meta:
        widgets = {"wireless_node_map": LeafletWidget()}

    def __init__(self, *args, **kwargs):
        super(wifistack_edit_node_form, self).__init__(*args, **kwargs)

        own_org_tag = self.user.userldapattributes.json_data["extensionAttribute1"][0]
        own_company = MdatCustomers.objects.get(org_tag=own_org_tag)

        domain_list = [
            domain["domain"] for domain in wifistack_get_domains(own_company.id)
        ]
        self.fields["wireless_node_domain"].choices = [
            (domain, domain) for domain in domain_list
        ]


class wifistack_claim_node_form(UserKwargModelFormMixin, forms.Form):
    hostname = forms.CharField(
        label=_("Hostname"),
        widget=forms.TextInput(attrs={"class": "form-control"}),
    )
    wireless_node_domain = forms.ChoiceField(
        label=_("Domain"),
        widget=forms.Select(attrs={"class": "form-control"}),
    )

    def __init__(self, *args, **kwargs):
        super(wifistack_claim_node_form, self).__init__(*args, **kwargs)

        # make sure, under no circumstances, user is None
        assert self.user is not None

        own_org_tag = self.user.userldapattributes.json_data["extensionAttribute1"][0]
        own_company = MdatCustomers.objects.get(org_tag=own_org_tag)

        domain_list = [
            domain["domain"] for domain in wifistack_get_domains(own_company.id)
        ]
        self.fields["wireless_node_domain"].choices = [
            (domain, domain) for domain in domain_list
        ]
