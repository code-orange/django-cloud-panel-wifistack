from django.urls import path

from . import views

urlpatterns = [
    path("nodes", views.list_nodes),
    path("nodes/claim", views.modal_claim_node),
    path("node/<int:node_id>", views.edit_node),
    path("node/<int:node_id>/code-dump", views.modal_dump_info),
]
